var sliderPeriod    = 5000;
var sliderTimer     = null;

(function($) {

    $(document).ready(function() {

        $('.slider').each(function() {
            var curSlider = $(this);
            curSlider.data('curIndex', 0);
            curSlider.data('disableAnimation', true);
            curSlider.find('.slider-preview li:first').addClass('active');
            curSlider.find('.slider-content li:first').css({'z-index': 2, 'left': 0, 'top': 0});
            curSlider.find('.slider-preview li:first').find('.slider-preview-load').animate({'width': curSlider.find('.slider-preview li:first a').outerWidth() - 2}, sliderPeriod, 'linear');
            sliderTimer = window.setTimeout(sliderNext, sliderPeriod);
        });

        function sliderNext() {
            var curSlider = $('.slider');

            if (curSlider.data('disableAnimation')) {
                var curIndex = curSlider.data('curIndex');
                var newIndex = curIndex + 1;
                if (newIndex >= curSlider.find('.slider-content li').length) {
                    newIndex = 0;
                }

                curSlider.data('curIndex', newIndex);
                curSlider.data('disableAnimation', false);

                curSlider.find('.slider-content li').eq(curIndex).css({'z-index': 2});
                curSlider.find('.slider-content li').eq(newIndex).css({'z-index': 1, 'left': 0, 'top': 0}).show();

                curSlider.find('.slider-preview li.active').removeClass('active');
                curSlider.find('.slider-preview li').eq(newIndex).addClass('active');
                curSlider.find('.slider-preview .slider-preview-load').stop(true, true).css({'width': 0});

                curSlider.find('.slider-content li').eq(curIndex).fadeOut(function() {
                    curSlider.find('.slider-preview li').eq(newIndex).find('.slider-preview-load').animate({'width': curSlider.find('.slider-preview li').eq(newIndex).find('a').outerWidth() - 2}, sliderPeriod, 'linear');
                    curSlider.data('disableAnimation', true);
                    sliderTimer = window.setTimeout(sliderNext, sliderPeriod);
                });
            }
        }

        $('.slider-preview a').click(function(e) {
            var curLi = $(this).parent();

            if (!curLi.hasClass('active')) {
                var curSlider = $('.slider');

                if (curSlider.data('disableAnimation')) {
                    window.clearTimeout(sliderTimer);
                    sliderTimer = null;

                    var curIndex = curSlider.data('curIndex');
                    var newIndex = curSlider.find('.slider-preview li').index(curLi);

                    curSlider.data('curIndex', newIndex);
                    curSlider.data('disableAnimation', false);

                    curSlider.find('.slider-content li').eq(curIndex).css({'z-index': 2});
                    curSlider.find('.slider-content li').eq(newIndex).css({'z-index': 1, 'left': 0, 'top': 0}).show();

                    curSlider.find('.slider-preview li.active').removeClass('active');
                    curSlider.find('.slider-preview li').eq(newIndex).addClass('active');
                    curSlider.find('.slider-preview .slider-preview-load').stop(true, true).css({'width': 0});

                    curSlider.find('.slider-content li').eq(curIndex).fadeOut(function() {
                        curSlider.find('.slider-preview li').eq(newIndex).find('.slider-preview-load').animate({'width': curSlider.find('.slider-preview li').eq(newIndex).find('a').outerWidth() - 2}, sliderPeriod, 'linear');
                        curSlider.data('disableAnimation', true);
                        sliderTimer = window.setTimeout(sliderNext, sliderPeriod);
                    });
                }
            }

            e.preventDefault();
        });

        $('.manufacturers-more-open a').click(function(e) {
            $('.manufacturers-more-open').hide();
            $('.manufacturers-more-close').show();
            $('.manufacturers-list').slideDown();
            e.preventDefault();
        });

        $('.manufacturers-more-close a').click(function(e) {
            $('.manufacturers-more-close').hide();
            $('.manufacturers-more-open').show();
            $('.manufacturers-list').slideUp();
            e.preventDefault();
        });

        $('.manufacturers-list-letters a').hover(
            function() {
                var curLetter = $(this).data('letter');
                if (curLetter == '') {
                    $('.manufacturers-list-letter div, .manufacturers-list-letter ul').addClass('hover');
                    $('.manufacturers-list-letter').addClass('hover');
                } else {
                    $('.manufacturers-list-letter div[data-letter="' + curLetter + '"], .manufacturers-list-letter ul[data-letter="' + curLetter + '"]').addClass('hover');
                    $('.manufacturers-list-letter ul[data-letter="' + curLetter + '"]').parent().addClass('hover');
                }
            },

            function() {
                $('.manufacturers-list-letter div, .manufacturers-list-letter ul').removeClass('hover');
                $('.manufacturers-list-letter').removeClass('hover');
            }
        );

        $('.manufacturers-list-letters a').click(function(e) {
            var curLink = $(this);
            var curLi = curLink.parent();
            if (curLi.hasClass('active')) {
                curLi.removeClass('active');
                $('.manufacturers-list-letter div, .manufacturers-list-letter ul').removeClass('active');
                $('.manufacturers-list-letter').removeClass('active');
            } else {
                $('.manufacturers-list-letters li.active').removeClass('active');
                curLi.addClass('active');
                var curLetter = curLink.data('letter');
                $('.manufacturers-list-letter div, .manufacturers-list-letter ul').removeClass('active');
                $('.manufacturers-list-letter').removeClass('active');
                $('.manufacturers-list-letter div[data-letter="' + curLetter + '"], .manufacturers-list-letter ul[data-letter="' + curLetter + '"]').addClass('active');
                $('.manufacturers-list-letter ul[data-letter="' + curLetter + '"]').parent().addClass('active');
            }
            e.preventDefault();
        });

        $('.main-catalogue-tabs a').click(function(e) {
            var curLi = $(this).parent();
            var curBlock = curLi.parents().filter('.main-catalogue');
            if (!curLi.hasClass('active')) {
                var curIndex = curBlock.find('.main-catalogue-tabs li').index(curLi);
                curBlock.find('.main-catalogue-tabs li.active').removeClass('active');
                curLi.addClass('active');

                curBlock.find('.main-catalogue-list').stop(true, true)
                curBlock.find('.main-catalogue-list:visible').fadeOut(function() {
                    curBlock.find('.main-catalogue-list').eq(curIndex).fadeIn();
                });
            }
            e.preventDefault();
        });

        $('input.maskPhone').mask('+7 (999) 999-99-99');

        $.extend($.validator.messages, {
            required: 'Не заполнено поле',
            email: 'Введен некорректный e-mail'
        });

        $('.form-select select, .filter-select select').chosen({disable_search: true, placeholder_text_multiple: ' ', no_results_text: 'Не найдено вариантов'});

        $('form').each(function() {
            $(this).validate({
              invalidHandler: function(form, validatorcalc) {
                  validatorcalc.showErrors();
                  $('.form-file').each(function() {
                      var curField = $(this);
                      if (curField.find('label.error').length > 0) {
                          curField.after(curField.find('label.error').clone());
                          curField.find('label.error').remove();
                      }
                  });
              }
            });
        });

        $('.form-checkbox span input:checked').parent().addClass('checked');
        $('.form-checkbox').click(function() {
            $(this).find('span').toggleClass('checked');
            $(this).find('input').prop('checked', $(this).find('span').hasClass('checked')).trigger('change');
        });

        $('.form-radio span input:checked').parent().addClass('checked');
        $('.form-radio').click(function() {
            var curName = $(this).find('input').attr('name');
            $('.form-radio input[name="' + curName + '"]').parent().removeClass('checked');
            $(this).find('span').addClass('checked');
            $(this).find('input').prop('checked', true).trigger('change');
        });

        $('.form-file input').change(function() {
            $(this).parent().parent().find('.form-file-title').html($(this).val().replace(/.*(\/|\\)/, '')).show();
            $(this).parent().parent().parent().find('label.error').remove();
        });

        $('.link-more').click(function(e) {
            var curLink = $(this);
            var curText = curLink.html();
            curLink.html(curLink.attr('rel'));
            curLink.attr('rel', curText);
            curLink.toggleClass('active');

            $('#' + curLink.attr('rev')).slideToggle();

            e.preventDefault();
        });

        $('.filter-checkbox input:checked').parent().addClass('checked');
        $('.filter-checkbox').click(function() {
            $(this).toggleClass('checked');
            $(this).find('input').prop('checked', $(this).hasClass('checked')).trigger('change');
        });

        $('.filter-reset input').click(function() {
            window.setTimeout(function() {
                $('.filter-checkbox.checked').removeClass('checked');
                $('.filter-checkbox input:checked').parent().addClass('checked');
            }, 100);
        });

        $('.filter-show a').click(function(e) {
            var curLink = $(this);
            var curText = curLink.html();
            curLink.html(curLink.attr('rel'));
            curLink.attr('rel', curText);

            $('.filter').toggleClass('open');

            e.preventDefault();
        });

        if ($('.individual').length > 0) {
            $(window).load(function() {
                var newHTML = '<ul>';
                $('.individual-gallery a').each(function() {
                    var curLink = $(this);
                    newHTML += '<li><a href="' + curLink.attr('href') + '" title="' + curLink.attr('title') + '"><img src="' + curLink.attr('rel') + '" alt="" width="140" height="104" /></a></li>';
                });
                newHTML += '</ul>';
                $('.item-gallery-list').prepend(newHTML);
                $('.item-gallery-list li:first').addClass('active');

                $('.item-gallery-list').each(function() {
                    var curSlider = $(this);
                    curSlider.data('curIndex', 0);
                    curSlider.data('disableAnimation', true);

                    curSlider.find('.item-gallery-list-prev').css({'display': 'none'});
                    if (curSlider.find('li').length < 6) {
                        curSlider.find('.item-gallery-list-next').css({'display': 'none'});
                    }

                    curSlider.find('ul').width(160 * curSlider.find('li').length);
                });

            });
        }

        $('.individual-gallery a').click(function(e) {
            var windowWidth     = $(window).width();
            var windowHeight    = $(window).height();
            var curScrollTop    = $(window).scrollTop();

            $('body').css({'width': windowWidth, 'height': windowHeight, 'overflow': 'hidden'});
            $(window).scrollTop(0);
            $('.wrapper').css({'margin-top': -curScrollTop});
            $('.wrapper').data('scrollTop', curScrollTop);

            var curIndex = $('.individual-gallery a').index($(this));
            $('.item-gallery-list ul li a').eq(curIndex).click();

            $('.item-gallery').addClass('item-gallery-open');

            e.preventDefault();
        });

        $('.item-gallery-close').click(function(e) {
            itemGalleryClose();
            e.preventDefault();
        });

        $('body').keyup(function(e) {
            if (e.keyCode == 27) {
                itemGalleryClose();
            }
        });

        function itemGalleryClose() {
            if ($('.item-gallery-open').length > 0) {
                $('.wrapper').css({'margin-top': '0'});
                $('body').css({'width': 'auto', 'height': '100%', 'overflow': 'visible'});
                $(window).scrollTop($('.wrapper').data('scrollTop'));

                $('.item-gallery').removeClass('item-gallery-open');
            }
        }

        $('.item-gallery').on('click', '.item-gallery-list ul li a', function(e) {
            $('.item-gallery-loading').show();
            var curLink = $(this);
            var curLi   = curLink.parent();

            $('.item-gallery-title').html(curLink.attr('title'));

            var curIndex = $('.item-gallery-list ul li').index(curLi);
            $('.item-gallery-load img').attr('src', curLink.attr('href'));
            $('.item-gallery-load img').load(function() {
                $('.item-gallery-big img').attr('src', curLink.attr('href'));

                $('.item-gallery-big strong').css({'visibility': 'visible'});

                $('.item-gallery-loading').hide();

                var contentHeight = $(window).height() - $('.item-gallery-container').height();
                if (contentHeight > 0) {
                    $('.item-gallery-container').css({'top': contentHeight / 2 + 'px'});
                } else {
                    $('.item-gallery-container').css({'top': '60px'});
                }
            });
            $('.item-gallery-list ul li.active').removeClass('active');
            curLi.addClass('active');

            e.preventDefault();
        });

        $('.item-gallery-prev').click(function(e) {
            var curIndex = $('.item-gallery-list ul li').index($('.item-gallery-list ul li.active'));
            curIndex--;
            if (curIndex < 0) {
                curIndex = $('.item-gallery-list ul li').length - 1;
            }
            $('.item-gallery-list ul li').eq(curIndex).find('a').click();
            if (curIndex < $('.item-gallery-list').data('curIndex') + 1) {
                $('.item-gallery-list-prev').click();
            }

            e.preventDefault();
        });

        $('.item-gallery-next').click(function(e) {
            var curIndex = $('.item-gallery-list ul li').index($('.item-gallery-list ul li.active'));
            curIndex++;
            if (curIndex >= $('.item-gallery-list ul li').length) {
                curIndex = 0;
            }
            $('.item-gallery-list ul li').eq(curIndex).find('a').click();
            if (curIndex > $('.item-gallery-list').data('curIndex') + 4) {
                $('.item-gallery-list-next').click();
            }

            e.preventDefault();
        });

        $('.item-gallery-list-next').click(function(e) {
            var curSlider = $('.item-gallery-list');

            if (curSlider.data('disableAnimation')) {
                var curIndex = curSlider.data('curIndex');
                curIndex += 5;
                curSlider.find('.item-gallery-list-prev').css({'display': 'block'});
                if (curIndex >= curSlider.find('li').length - 5) {
                    curIndex = curSlider.find('li').length - 5;
                    curSlider.find('.item-gallery-list-next').css({'display': 'none'});
                }

                curSlider.data('disableAnimation', false);
                curSlider.find('ul').animate({'left': -curIndex * 160}, function() {
                    curSlider.data('curIndex', curIndex);
                    curSlider.data('disableAnimation', true);
                });
            }

            e.preventDefault();
        });

        $('.item-gallery-list-prev').click(function(e) {
            var curSlider = $('.item-gallery-list');

            if (curSlider.data('disableAnimation')) {
                var curIndex = curSlider.data('curIndex');
                curIndex -= 5;
                curSlider.find('.item-gallery-list-next').css({'display': 'block'});
                if (curIndex <= 0) {
                    curIndex = 0;
                    curSlider.find('.item-gallery-list-prev').css({'display': 'none'});
                }

                curSlider.data('disableAnimation', false);
                curSlider.find('ul').animate({'left': -curIndex * 160}, function() {
                    curSlider.data('curIndex', curIndex);
                    curSlider.data('disableAnimation', true);
                });
            }

            e.preventDefault();
        });

        (function() {

            var fancybox_after_load = function() {
                this.title = '<div class="title-extra-wrapper">' + this.title + '</div>';
            };
            var fancybox_after_show = function () {
                var self = this;
                // At first time - draw controls
                if ($(".fancybox-thumb-control").length == 0) {
                    
                    setTimeout(function () {

                        var $ul = $("#fancybox-thumbs").find('ul');
                        $ul.attr('data-pos', 0);
                        $ul.addClass('pos-0');

                        var max_pos;
                        if ( $(self.element).attr('rel') && $(self.element).attr('rel') != 'fancybox-thumb') {
                            max_pos = $('.fancygallery--' + $(self.element).attr('rel')).find(".fancygallery__preview").length;
                        } else {
                            max_pos = $('.fancygallery__thumb-callers').find('a').length;
                        }
                        var n_pos = max_pos > 5 ? 5 : max_pos;

                        $ul.attr('data-max-pos', max_pos);
                        $ul.attr('data-n-pos', n_pos);

                        if ( n_pos < max_pos ) { 
                            $("<a href='#' class='fancybox-thumb-control prev disabled'><span></span></a>").insertAfter("#fancybox-thumbs");
                            $("<a href='#' class='fancybox-thumb-control next'><span></span></a>").insertAfter("#fancybox-thumbs");
                        } else { 
                            $("#fancybox-thumbs").css({ 
                                'width' : n_pos * 111,
                                'margin-left' : -111 * n_pos/2
                            });
                        }
                    }, 100);
                };

                // Move thumbs if needed
                setTimeout(function(){
                    var $t   = $("#fancybox-thumbs"),
                        $ul  = $t.find('ul'),
                        $lis = $t.find("li"),
                        $lia = $t.find("li.active"),
                        idx  = Number( $lis.index( $lia ) ),
                        pos  = Number( $ul.attr('data-pos') ),
                        max_pos  = Number( $ul.attr('data-max-pos') );

                    var visible_l = pos, visible_r = pos + 4;
                    var dx = 0;
                    if ( pos > max_pos ) pos = max_pos;

                    if ( idx > visible_r ) {
                        dx = idx - visible_r;
                    } else if ( idx < visible_l ) {
                        dx = idx -  visible_l;
                    } else {
                        dx = 0;
                    }

                    if ( dx ) ThumbShifter.goTo( $ul, pos + dx);
                }, 110);

            };
            var fancybox_after_close = function () {
                $(".fancybox-thumb-control").remove();
            };
            var FancyConfig = function(current_id) {
                var cfg = {
                    padding: 0,
                    prevEffect  : 'none',
                    nextEffect  : 'none',
                    helpers : {
                        title   : {
                            type: 'inside'
                        },
                        thumbs  : {
                            width   : 100,
                            height  : 100,
                            source  : function( item ) {
                                return item.href;
                            }
                        },
                        overlay: {
                           locked: false
                        }
                    },
                    afterLoad: fancybox_after_load,
                    afterShow: fancybox_after_show,
                    afterClose: fancybox_after_close
                };
                if (current_id) cfg.index = current_id;
                return cfg;
            }

            $(".fancygallery__caller").on('click', function(){
                var $this = $(this);
                var $gallery = $this.parents('.fancygallery');
                var $img = $gallery.find('img.fancygallery__main-image');

                var current_id = $img.attr('data-id') ? Number($img.attr('data-id')) : 0;
                $.fancybox(Images, FancyConfig(current_id) );

                return false;
            });

            $(".fancybox-thumb.autofancy").fancybox(FancyConfig());

            $(".fancygallery__go").on('click', function(){
                var $this = $(this);
                var $gallery = $this.parents('.fancygallery');
                var $img = $gallery.find('img.fancygallery__main-image');
                var max = $gallery.find('ul').find('li').length;
                var current_id = $img.attr('data-id') ? Number($img.attr('data-id')) : 0;
                var next_id;

                if ( ($this).hasClass('fancygallery__go--prev') ) next_id = current_id - 1;
                if ( ($this).hasClass('fancygallery__go--next') ) next_id = current_id + 1;

                if ( next_id < 0 ) { next_id = max - 1; }
                if ( next_id >= max ) { next_id = 0; }

                $img.attr('data-id', next_id);

                var $imgs = $gallery.find('.fancygallery__thumb-callers').find('a');
                var $lis = $gallery.find('.fancygallery__thumb-callers').find('ul').find('li');

                $lis.removeClass('active');
                $lis.eq(next_id).addClass('active');
                $img.attr('src', $imgs.eq(next_id).attr('data-medium') || $imgs.eq(next_id).attr('href') );

                return false;
            });

            $('.fancygallery__thumb-callers').find('ul').find('li').on('click', function(){
                var $this = $(this),
                    $parent = $this.parent(),
                    $gallery = $this.parents('.fancygallery'),
                    $img = $gallery.find('img.fancygallery__main-image'),
                    $imgs = $gallery.find('.fancygallery__thumb-callers').find('a'),
                    $lis = $parent.find('li'),
                    next_id = $this.index();

                $lis.removeClass('active');
                $this.addClass('active');
                $img.attr('data-id', next_id);
                $img.attr('src', $imgs.eq(next_id).attr('data-medium') || $imgs.eq(next_id).attr('href') );

                return false;
            });

            // thumb actions
            var ThumbShifter = (function(){
                var TS = {};

                TS.checkArrowStatus = function( $ul ) {

                    var pos = Number($ul.attr('data-pos'));
                    var max_pos = Number( $ul.attr('data-max-pos') );
                    var n_pos = Number( $ul.attr('data-n-pos') );

                    if ( pos == max_pos-n_pos ) {
                        $('.fancybox-thumb-control.next').addClass('disabled');
                    } else {
                        $('.fancybox-thumb-control.next').removeClass('disabled');
                    }
                    if (  pos== 0 ) {
                        $('.fancybox-thumb-control.prev').addClass('disabled');
                    } else {
                        $('.fancybox-thumb-control.prev').removeClass('disabled');
                    }
                };

                TS.goLeft = function ( $ul ) {
                    var pos = Number( $ul.attr('data-pos') );
                    var max_pos = Number( $ul.attr('data-max-pos') );
                    var n_pos = Number( $ul.attr('data-n-pos') );

                    if ( pos == 0 ) {
                        ThumbShifter.goTo( $ul, max_pos-n_pos );
                        pos -= 1;
                        $ul.attr('data-pos', pos);
                    } else {
                        $ul.removeClass( 'pos-' + pos );
                        $ul.addClass( 'pos-' + Number(pos - 1) );
                        pos -= 1;
                        $ul.attr('data-pos', pos);
                        ThumbShifter.checkArrowStatus($ul);
                    }
                };
                TS.goRight = function ( $ul ) {
                    var pos = Number( $ul.attr('data-pos') );
                    var max_pos = Number( $ul.attr('data-max-pos') );
                    var n_pos = Number( $ul.attr('data-n-pos') );
                    var t = max_pos-n_pos;

                    if ( pos == t ) {
                        ThumbShifter.goTo($ul, 0);
                        pos += 1;
                        $ul.attr('data-pos', pos);
                    } else {
                        $ul.removeClass( 'pos-' + pos );
                        $ul.addClass( 'pos-' + Number(pos + 1) );
                        pos += 1;
                        $ul.attr('data-pos', pos);
                        ThumbShifter.checkArrowStatus($ul);
                    }

                };
                TS.goTo = function( $ul, pos_goto ) {
                    var pos = Number( $ul.attr('data-pos') );

                    $ul.removeClass( 'pos-' + pos );
                    $ul.addClass( 'pos-' + Number(pos_goto) );
                    $ul.attr('data-pos', Number(pos_goto) );

                    TS.checkArrowStatus($ul);
                }

                return TS;
            })();

            var Images = [];
            $.each($('.fancybox-thumb'), function(){
                Images.push({
                    href : $(this).attr('href'),
                    title : $(this).attr('title')
                });
            });

            // thumb buttons behaviour
            $(document).on('click', ".fancybox-thumb-control", function(){
                var $ul = $("#fancybox-thumbs").find('ul'),
                    max_pos = Number( $ul.attr('data-max-pos') ),
                    n_pos = Number( $ul.attr('data-n-pos') );

                if ( $(this).hasClass('disabled') ) {
                    return false;
                }

                if ( $(this).hasClass('prev') ) {
                    // go left
                    ThumbShifter.goLeft ( $ul );
                } else {
                    // go right
                    ThumbShifter.goRight ( $ul );
                }
               
                return false;
            });

        })()

    });

})(jQuery);
